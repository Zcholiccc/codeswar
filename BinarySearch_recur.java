import java.util.*;

public class BinarySearch_recur{
	public static int rank(int[] n ,int target,int lo,int hi){
		if(target==n[(lo+hi)/2]){
			return (lo+hi)/2;
		}
		
		if(lo==hi){
			return -1;
		}
		
		else if (target>n[(lo+hi)/2]){
			return rank(n,target,(hi+lo)/2+1,hi);
		}
		
		else{
			return rank(n,target,lo,(hi+lo)/2-1);
		}
	}
	
	public static void 
	
	
	public static void main(String[] args){
		Scanner keyboard=new Scanner(System.in);
		int n=keyboard.nextInt();
		int[] a=new int[n];
		for(int i=0;i<n;i++){
			a[i]=keyboard.nextInt();
		}
		Arrays.sort(a);
		System.out.println("What number do you want?");
		int match=keyboard.nextInt();
		System.out.println(rank(a,match,0,a.length-1));
	}
}


